gdjs._492Code = {};
gdjs._492Code.GDFONDObjects1= [];
gdjs._492Code.GDFONDObjects2= [];
gdjs._492Code.GDCIBLEOBJET2Objects1= [];
gdjs._492Code.GDCIBLEOBJET2Objects2= [];
gdjs._492Code.GDCIBLEOBJET3Objects1= [];
gdjs._492Code.GDCIBLEOBJET3Objects2= [];
gdjs._492Code.GDCIBLEOBJET4Objects1= [];
gdjs._492Code.GDCIBLEOBJET4Objects2= [];
gdjs._492Code.GDCIBLEOBJET1Objects1= [];
gdjs._492Code.GDCIBLEOBJET1Objects2= [];
gdjs._492Code.GDOBJET2Objects1= [];
gdjs._492Code.GDOBJET2Objects2= [];
gdjs._492Code.GDOBJET3Objects1= [];
gdjs._492Code.GDOBJET3Objects2= [];
gdjs._492Code.GDOBJET4Objects1= [];
gdjs._492Code.GDOBJET4Objects2= [];
gdjs._492Code.GDOBJET1Objects1= [];
gdjs._492Code.GDOBJET1Objects2= [];
gdjs._492Code.GDBACKObjects1= [];
gdjs._492Code.GDBACKObjects2= [];
gdjs._492Code.GDRETOURSOMMAIREObjects1= [];
gdjs._492Code.GDRETOURSOMMAIREObjects2= [];

gdjs._492Code.conditionTrue_0 = {val:false};
gdjs._492Code.condition0IsTrue_0 = {val:false};
gdjs._492Code.condition1IsTrue_0 = {val:false};
gdjs._492Code.condition2IsTrue_0 = {val:false};
gdjs._492Code.condition3IsTrue_0 = {val:false};
gdjs._492Code.condition4IsTrue_0 = {val:false};
gdjs._492Code.condition5IsTrue_0 = {val:false};
gdjs._492Code.conditionTrue_1 = {val:false};
gdjs._492Code.condition0IsTrue_1 = {val:false};
gdjs._492Code.condition1IsTrue_1 = {val:false};
gdjs._492Code.condition2IsTrue_1 = {val:false};
gdjs._492Code.condition3IsTrue_1 = {val:false};
gdjs._492Code.condition4IsTrue_1 = {val:false};
gdjs._492Code.condition5IsTrue_1 = {val:false};


gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._492Code.GDRETOURSOMMAIREObjects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET1Objects1Objects = Hashtable.newFrom({"OBJET1": gdjs._492Code.GDOBJET1Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET1Objects1Objects = Hashtable.newFrom({"CIBLEOBJET1": gdjs._492Code.GDCIBLEOBJET1Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET2Objects1Objects = Hashtable.newFrom({"OBJET2": gdjs._492Code.GDOBJET2Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET2Objects1Objects = Hashtable.newFrom({"CIBLEOBJET2": gdjs._492Code.GDCIBLEOBJET2Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET3Objects1Objects = Hashtable.newFrom({"OBJET3": gdjs._492Code.GDOBJET3Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET3Objects1Objects = Hashtable.newFrom({"CIBLEOBJET3": gdjs._492Code.GDCIBLEOBJET3Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET4Objects1Objects = Hashtable.newFrom({"OBJET4": gdjs._492Code.GDOBJET4Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET4Objects1Objects = Hashtable.newFrom({"CIBLEOBJET4": gdjs._492Code.GDCIBLEOBJET4Objects1});gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._492Code.GDBACKObjects1});gdjs._492Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._492Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._492Code.condition0IsTrue_0.val = false;
gdjs._492Code.condition1IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._492Code.condition0IsTrue_0.val ) {
{
gdjs._492Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._492Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{


gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._492Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "13", false);
}}

}


{


gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._492Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{

gdjs._492Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));
gdjs._492Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));
gdjs._492Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));
gdjs._492Code.GDOBJET4Objects1.createFrom(runtimeScene.getObjects("OBJET4"));

gdjs._492Code.condition0IsTrue_0.val = false;
gdjs._492Code.condition1IsTrue_0.val = false;
gdjs._492Code.condition2IsTrue_0.val = false;
gdjs._492Code.condition3IsTrue_0.val = false;
gdjs._492Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs._492Code.GDOBJET1Objects1.length;i<l;++i) {
    if ( !(gdjs._492Code.GDOBJET1Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._492Code.condition0IsTrue_0.val = true;
        gdjs._492Code.GDOBJET1Objects1[k] = gdjs._492Code.GDOBJET1Objects1[i];
        ++k;
    }
}
gdjs._492Code.GDOBJET1Objects1.length = k;}if ( gdjs._492Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._492Code.GDOBJET2Objects1.length;i<l;++i) {
    if ( !(gdjs._492Code.GDOBJET2Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._492Code.condition1IsTrue_0.val = true;
        gdjs._492Code.GDOBJET2Objects1[k] = gdjs._492Code.GDOBJET2Objects1[i];
        ++k;
    }
}
gdjs._492Code.GDOBJET2Objects1.length = k;}if ( gdjs._492Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._492Code.GDOBJET3Objects1.length;i<l;++i) {
    if ( !(gdjs._492Code.GDOBJET3Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._492Code.condition2IsTrue_0.val = true;
        gdjs._492Code.GDOBJET3Objects1[k] = gdjs._492Code.GDOBJET3Objects1[i];
        ++k;
    }
}
gdjs._492Code.GDOBJET3Objects1.length = k;}if ( gdjs._492Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._492Code.GDOBJET4Objects1.length;i<l;++i) {
    if ( !(gdjs._492Code.GDOBJET4Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._492Code.condition3IsTrue_0.val = true;
        gdjs._492Code.GDOBJET4Objects1[k] = gdjs._492Code.GDOBJET4Objects1[i];
        ++k;
    }
}
gdjs._492Code.GDOBJET4Objects1.length = k;}if ( gdjs._492Code.condition3IsTrue_0.val ) {
{
{gdjs._492Code.conditionTrue_1 = gdjs._492Code.condition4IsTrue_0;
gdjs._492Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6901820);
}
}}
}
}
}
if (gdjs._492Code.condition4IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{

gdjs._492Code.GDCIBLEOBJET1Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET1"));
gdjs._492Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));

gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET1Objects1Objects, gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET1Objects1Objects, false, runtimeScene, false);
}if (gdjs._492Code.condition0IsTrue_0.val) {
/* Reuse gdjs._492Code.GDOBJET1Objects1 */
{for(var i = 0, len = gdjs._492Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET1Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._492Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET1Objects1[i].setPosition(80,168);
}
}}

}


{

gdjs._492Code.GDCIBLEOBJET2Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET2"));
gdjs._492Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));

gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET2Objects1Objects, gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET2Objects1Objects, false, runtimeScene, false);
}if (gdjs._492Code.condition0IsTrue_0.val) {
/* Reuse gdjs._492Code.GDOBJET2Objects1 */
{for(var i = 0, len = gdjs._492Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET2Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._492Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET2Objects1[i].setPosition(680,207);
}
}}

}


{

gdjs._492Code.GDCIBLEOBJET3Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET3"));
gdjs._492Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET3Objects1Objects, gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET3Objects1Objects, false, runtimeScene, false);
}if (gdjs._492Code.condition0IsTrue_0.val) {
/* Reuse gdjs._492Code.GDOBJET3Objects1 */
{for(var i = 0, len = gdjs._492Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET3Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._492Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET3Objects1[i].setPosition(320,368);
}
}}

}


{

gdjs._492Code.GDCIBLEOBJET4Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET4"));
gdjs._492Code.GDOBJET4Objects1.createFrom(runtimeScene.getObjects("OBJET4"));

gdjs._492Code.condition0IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDOBJET4Objects1Objects, gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDCIBLEOBJET4Objects1Objects, false, runtimeScene, false);
}if (gdjs._492Code.condition0IsTrue_0.val) {
/* Reuse gdjs._492Code.GDOBJET4Objects1 */
{for(var i = 0, len = gdjs._492Code.GDOBJET4Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET4Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._492Code.GDOBJET4Objects1.length ;i < len;++i) {
    gdjs._492Code.GDOBJET4Objects1[i].setPosition(325,574);
}
}}

}


{

gdjs._492Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._492Code.condition0IsTrue_0.val = false;
gdjs._492Code.condition1IsTrue_0.val = false;
{
gdjs._492Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._492Code.mapOfGDgdjs_46_95492Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._492Code.condition0IsTrue_0.val ) {
{
gdjs._492Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._492Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._492Code.eventsList0x5b71c8


gdjs._492Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._492Code.GDFONDObjects1.length = 0;
gdjs._492Code.GDFONDObjects2.length = 0;
gdjs._492Code.GDCIBLEOBJET2Objects1.length = 0;
gdjs._492Code.GDCIBLEOBJET2Objects2.length = 0;
gdjs._492Code.GDCIBLEOBJET3Objects1.length = 0;
gdjs._492Code.GDCIBLEOBJET3Objects2.length = 0;
gdjs._492Code.GDCIBLEOBJET4Objects1.length = 0;
gdjs._492Code.GDCIBLEOBJET4Objects2.length = 0;
gdjs._492Code.GDCIBLEOBJET1Objects1.length = 0;
gdjs._492Code.GDCIBLEOBJET1Objects2.length = 0;
gdjs._492Code.GDOBJET2Objects1.length = 0;
gdjs._492Code.GDOBJET2Objects2.length = 0;
gdjs._492Code.GDOBJET3Objects1.length = 0;
gdjs._492Code.GDOBJET3Objects2.length = 0;
gdjs._492Code.GDOBJET4Objects1.length = 0;
gdjs._492Code.GDOBJET4Objects2.length = 0;
gdjs._492Code.GDOBJET1Objects1.length = 0;
gdjs._492Code.GDOBJET1Objects2.length = 0;
gdjs._492Code.GDBACKObjects1.length = 0;
gdjs._492Code.GDBACKObjects2.length = 0;
gdjs._492Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._492Code.GDRETOURSOMMAIREObjects2.length = 0;

gdjs._492Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_492Code'] = gdjs._492Code;
