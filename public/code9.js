gdjs._490Code = {};
gdjs._490Code.GDFONDObjects1= [];
gdjs._490Code.GDFONDObjects2= [];
gdjs._490Code.GDBACKObjects1= [];
gdjs._490Code.GDBACKObjects2= [];
gdjs._490Code.GDRETOURSOMMAIREObjects1= [];
gdjs._490Code.GDRETOURSOMMAIREObjects2= [];

gdjs._490Code.conditionTrue_0 = {val:false};
gdjs._490Code.condition0IsTrue_0 = {val:false};
gdjs._490Code.condition1IsTrue_0 = {val:false};
gdjs._490Code.condition2IsTrue_0 = {val:false};


gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._490Code.GDRETOURSOMMAIREObjects1});gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDFONDObjects1Objects = Hashtable.newFrom({"FOND": gdjs._490Code.GDFONDObjects1});gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._490Code.GDBACKObjects1});gdjs._490Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._490Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._490Code.condition0IsTrue_0.val = false;
gdjs._490Code.condition1IsTrue_0.val = false;
{
gdjs._490Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._490Code.condition0IsTrue_0.val ) {
{
gdjs._490Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._490Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._490Code.GDFONDObjects1.createFrom(runtimeScene.getObjects("FOND"));

gdjs._490Code.condition0IsTrue_0.val = false;
gdjs._490Code.condition1IsTrue_0.val = false;
{
gdjs._490Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDFONDObjects1Objects, runtimeScene, true, false);
}if ( gdjs._490Code.condition0IsTrue_0.val ) {
{
gdjs._490Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._490Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "11", true);
}}

}


{

gdjs._490Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._490Code.condition0IsTrue_0.val = false;
gdjs._490Code.condition1IsTrue_0.val = false;
{
gdjs._490Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._490Code.mapOfGDgdjs_46_95490Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._490Code.condition0IsTrue_0.val ) {
{
gdjs._490Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._490Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._490Code.eventsList0x5b71c8


gdjs._490Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._490Code.GDFONDObjects1.length = 0;
gdjs._490Code.GDFONDObjects2.length = 0;
gdjs._490Code.GDBACKObjects1.length = 0;
gdjs._490Code.GDBACKObjects2.length = 0;
gdjs._490Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._490Code.GDRETOURSOMMAIREObjects2.length = 0;

gdjs._490Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_490Code'] = gdjs._490Code;
