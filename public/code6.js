gdjs._487Code = {};
gdjs._487Code.GDFONDObjects1= [];
gdjs._487Code.GDFONDObjects2= [];
gdjs._487Code.GDCIBLEPLAObjects1= [];
gdjs._487Code.GDCIBLEPLAObjects2= [];
gdjs._487Code.GDCIBLEPORCObjects1= [];
gdjs._487Code.GDCIBLEPORCObjects2= [];
gdjs._487Code.GDCIBLEMETALObjects1= [];
gdjs._487Code.GDCIBLEMETALObjects2= [];
gdjs._487Code.GDCIBLEBOISObjects1= [];
gdjs._487Code.GDCIBLEBOISObjects2= [];
gdjs._487Code.GDRETOURSOMMAIREObjects1= [];
gdjs._487Code.GDRETOURSOMMAIREObjects2= [];
gdjs._487Code.GDPLASTIQUEObjects1= [];
gdjs._487Code.GDPLASTIQUEObjects2= [];
gdjs._487Code.GDMObjects1= [];
gdjs._487Code.GDMObjects2= [];
gdjs._487Code.GDPORObjects1= [];
gdjs._487Code.GDPORObjects2= [];
gdjs._487Code.GDBACKObjects1= [];
gdjs._487Code.GDBACKObjects2= [];
gdjs._487Code.GDBOISObjects1= [];
gdjs._487Code.GDBOISObjects2= [];

gdjs._487Code.conditionTrue_0 = {val:false};
gdjs._487Code.condition0IsTrue_0 = {val:false};
gdjs._487Code.condition1IsTrue_0 = {val:false};
gdjs._487Code.condition2IsTrue_0 = {val:false};
gdjs._487Code.condition3IsTrue_0 = {val:false};
gdjs._487Code.condition4IsTrue_0 = {val:false};
gdjs._487Code.condition5IsTrue_0 = {val:false};
gdjs._487Code.conditionTrue_1 = {val:false};
gdjs._487Code.condition0IsTrue_1 = {val:false};
gdjs._487Code.condition1IsTrue_1 = {val:false};
gdjs._487Code.condition2IsTrue_1 = {val:false};
gdjs._487Code.condition3IsTrue_1 = {val:false};
gdjs._487Code.condition4IsTrue_1 = {val:false};
gdjs._487Code.condition5IsTrue_1 = {val:false};


gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._487Code.GDRETOURSOMMAIREObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDBOISObjects1Objects = Hashtable.newFrom({"BOIS": gdjs._487Code.GDBOISObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEBOISObjects1Objects = Hashtable.newFrom({"CIBLEBOIS": gdjs._487Code.GDCIBLEBOISObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDPLASTIQUEObjects1Objects = Hashtable.newFrom({"PLASTIQUE": gdjs._487Code.GDPLASTIQUEObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEBOISObjects1Objects = Hashtable.newFrom({"CIBLEBOIS": gdjs._487Code.GDCIBLEBOISObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDMObjects1Objects = Hashtable.newFrom({"M": gdjs._487Code.GDMObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEMETALObjects1Objects = Hashtable.newFrom({"CIBLEMETAL": gdjs._487Code.GDCIBLEMETALObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDPORObjects1Objects = Hashtable.newFrom({"POR": gdjs._487Code.GDPORObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEPORCObjects1Objects = Hashtable.newFrom({"CIBLEPORC": gdjs._487Code.GDCIBLEPORCObjects1});gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._487Code.GDBACKObjects1});gdjs._487Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._487Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._487Code.condition0IsTrue_0.val = false;
gdjs._487Code.condition1IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._487Code.condition0IsTrue_0.val ) {
{
gdjs._487Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._487Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._487Code.GDBOISObjects1.createFrom(runtimeScene.getObjects("BOIS"));
gdjs._487Code.GDCIBLEBOISObjects1.createFrom(runtimeScene.getObjects("CIBLEBOIS"));

gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDBOISObjects1Objects, gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEBOISObjects1Objects, false, runtimeScene, false);
}if (gdjs._487Code.condition0IsTrue_0.val) {
/* Reuse gdjs._487Code.GDBOISObjects1 */
{for(var i = 0, len = gdjs._487Code.GDBOISObjects1.length ;i < len;++i) {
    gdjs._487Code.GDBOISObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._487Code.GDBOISObjects1.length ;i < len;++i) {
    gdjs._487Code.GDBOISObjects1[i].setPosition(62,534);
}
}}

}


{

gdjs._487Code.GDCIBLEBOISObjects1.createFrom(runtimeScene.getObjects("CIBLEBOIS"));
gdjs._487Code.GDPLASTIQUEObjects1.createFrom(runtimeScene.getObjects("PLASTIQUE"));

gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDPLASTIQUEObjects1Objects, gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEBOISObjects1Objects, false, runtimeScene, false);
}if (gdjs._487Code.condition0IsTrue_0.val) {
/* Reuse gdjs._487Code.GDPLASTIQUEObjects1 */
{for(var i = 0, len = gdjs._487Code.GDPLASTIQUEObjects1.length ;i < len;++i) {
    gdjs._487Code.GDPLASTIQUEObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._487Code.GDPLASTIQUEObjects1.length ;i < len;++i) {
    gdjs._487Code.GDPLASTIQUEObjects1[i].setPosition(194,528);
}
}}

}


{

gdjs._487Code.GDCIBLEMETALObjects1.createFrom(runtimeScene.getObjects("CIBLEMETAL"));
gdjs._487Code.GDMObjects1.createFrom(runtimeScene.getObjects("M"));

gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDMObjects1Objects, gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEMETALObjects1Objects, false, runtimeScene, false);
}if (gdjs._487Code.condition0IsTrue_0.val) {
/* Reuse gdjs._487Code.GDMObjects1 */
{for(var i = 0, len = gdjs._487Code.GDMObjects1.length ;i < len;++i) {
    gdjs._487Code.GDMObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._487Code.GDMObjects1.length ;i < len;++i) {
    gdjs._487Code.GDMObjects1[i].setPosition(340,275);
}
}}

}


{

gdjs._487Code.GDCIBLEPORCObjects1.createFrom(runtimeScene.getObjects("CIBLEPORC"));
gdjs._487Code.GDPORObjects1.createFrom(runtimeScene.getObjects("POR"));

gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDPORObjects1Objects, gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDCIBLEPORCObjects1Objects, false, runtimeScene, false);
}if (gdjs._487Code.condition0IsTrue_0.val) {
/* Reuse gdjs._487Code.GDPORObjects1 */
{for(var i = 0, len = gdjs._487Code.GDPORObjects1.length ;i < len;++i) {
    gdjs._487Code.GDPORObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._487Code.GDPORObjects1.length ;i < len;++i) {
    gdjs._487Code.GDPORObjects1[i].setPosition(542,527);
}
}}

}


{

gdjs._487Code.GDBOISObjects1.createFrom(runtimeScene.getObjects("BOIS"));
gdjs._487Code.GDMObjects1.createFrom(runtimeScene.getObjects("M"));
gdjs._487Code.GDPLASTIQUEObjects1.createFrom(runtimeScene.getObjects("PLASTIQUE"));
gdjs._487Code.GDPORObjects1.createFrom(runtimeScene.getObjects("POR"));

gdjs._487Code.condition0IsTrue_0.val = false;
gdjs._487Code.condition1IsTrue_0.val = false;
gdjs._487Code.condition2IsTrue_0.val = false;
gdjs._487Code.condition3IsTrue_0.val = false;
gdjs._487Code.condition4IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs._487Code.GDPLASTIQUEObjects1.length;i<l;++i) {
    if ( !(gdjs._487Code.GDPLASTIQUEObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._487Code.condition0IsTrue_0.val = true;
        gdjs._487Code.GDPLASTIQUEObjects1[k] = gdjs._487Code.GDPLASTIQUEObjects1[i];
        ++k;
    }
}
gdjs._487Code.GDPLASTIQUEObjects1.length = k;}if ( gdjs._487Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._487Code.GDBOISObjects1.length;i<l;++i) {
    if ( !(gdjs._487Code.GDBOISObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._487Code.condition1IsTrue_0.val = true;
        gdjs._487Code.GDBOISObjects1[k] = gdjs._487Code.GDBOISObjects1[i];
        ++k;
    }
}
gdjs._487Code.GDBOISObjects1.length = k;}if ( gdjs._487Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._487Code.GDMObjects1.length;i<l;++i) {
    if ( !(gdjs._487Code.GDMObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._487Code.condition2IsTrue_0.val = true;
        gdjs._487Code.GDMObjects1[k] = gdjs._487Code.GDMObjects1[i];
        ++k;
    }
}
gdjs._487Code.GDMObjects1.length = k;}if ( gdjs._487Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._487Code.GDPORObjects1.length;i<l;++i) {
    if ( !(gdjs._487Code.GDPORObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._487Code.condition3IsTrue_0.val = true;
        gdjs._487Code.GDPORObjects1[k] = gdjs._487Code.GDPORObjects1[i];
        ++k;
    }
}
gdjs._487Code.GDPORObjects1.length = k;}if ( gdjs._487Code.condition3IsTrue_0.val ) {
{
{gdjs._487Code.conditionTrue_1 = gdjs._487Code.condition4IsTrue_0;
gdjs._487Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7237148);
}
}}
}
}
}
if (gdjs._487Code.condition4IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{


gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._487Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{


gdjs._487Code.condition0IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._487Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "08", false);
}}

}


{

gdjs._487Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._487Code.condition0IsTrue_0.val = false;
gdjs._487Code.condition1IsTrue_0.val = false;
{
gdjs._487Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._487Code.mapOfGDgdjs_46_95487Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._487Code.condition0IsTrue_0.val ) {
{
gdjs._487Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._487Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._487Code.eventsList0x5b71c8


gdjs._487Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._487Code.GDFONDObjects1.length = 0;
gdjs._487Code.GDFONDObjects2.length = 0;
gdjs._487Code.GDCIBLEPLAObjects1.length = 0;
gdjs._487Code.GDCIBLEPLAObjects2.length = 0;
gdjs._487Code.GDCIBLEPORCObjects1.length = 0;
gdjs._487Code.GDCIBLEPORCObjects2.length = 0;
gdjs._487Code.GDCIBLEMETALObjects1.length = 0;
gdjs._487Code.GDCIBLEMETALObjects2.length = 0;
gdjs._487Code.GDCIBLEBOISObjects1.length = 0;
gdjs._487Code.GDCIBLEBOISObjects2.length = 0;
gdjs._487Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._487Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._487Code.GDPLASTIQUEObjects1.length = 0;
gdjs._487Code.GDPLASTIQUEObjects2.length = 0;
gdjs._487Code.GDMObjects1.length = 0;
gdjs._487Code.GDMObjects2.length = 0;
gdjs._487Code.GDPORObjects1.length = 0;
gdjs._487Code.GDPORObjects2.length = 0;
gdjs._487Code.GDBACKObjects1.length = 0;
gdjs._487Code.GDBACKObjects2.length = 0;
gdjs._487Code.GDBOISObjects1.length = 0;
gdjs._487Code.GDBOISObjects2.length = 0;

gdjs._487Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_487Code'] = gdjs._487Code;
