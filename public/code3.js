gdjs._484Code = {};
gdjs._484Code.GDFONDObjects1= [];
gdjs._484Code.GDFONDObjects2= [];
gdjs._484Code.GDFONDObjects3= [];
gdjs._484Code.GDr2Objects1= [];
gdjs._484Code.GDr2Objects2= [];
gdjs._484Code.GDr2Objects3= [];
gdjs._484Code.GDr3Objects1= [];
gdjs._484Code.GDr3Objects2= [];
gdjs._484Code.GDr3Objects3= [];
gdjs._484Code.GDr1Objects1= [];
gdjs._484Code.GDr1Objects2= [];
gdjs._484Code.GDr1Objects3= [];
gdjs._484Code.GDflecher3Objects1= [];
gdjs._484Code.GDflecher3Objects2= [];
gdjs._484Code.GDflecher3Objects3= [];
gdjs._484Code.GDflecher2Objects1= [];
gdjs._484Code.GDflecher2Objects2= [];
gdjs._484Code.GDflecher2Objects3= [];
gdjs._484Code.GDflecher1Objects1= [];
gdjs._484Code.GDflecher1Objects2= [];
gdjs._484Code.GDflecher1Objects3= [];
gdjs._484Code.GDRETOURSOMMAIREObjects1= [];
gdjs._484Code.GDRETOURSOMMAIREObjects2= [];
gdjs._484Code.GDRETOURSOMMAIREObjects3= [];
gdjs._484Code.GDBACKObjects1= [];
gdjs._484Code.GDBACKObjects2= [];
gdjs._484Code.GDBACKObjects3= [];

gdjs._484Code.conditionTrue_0 = {val:false};
gdjs._484Code.condition0IsTrue_0 = {val:false};
gdjs._484Code.condition1IsTrue_0 = {val:false};
gdjs._484Code.condition2IsTrue_0 = {val:false};
gdjs._484Code.condition3IsTrue_0 = {val:false};
gdjs._484Code.conditionTrue_1 = {val:false};
gdjs._484Code.condition0IsTrue_1 = {val:false};
gdjs._484Code.condition1IsTrue_1 = {val:false};
gdjs._484Code.condition2IsTrue_1 = {val:false};
gdjs._484Code.condition3IsTrue_1 = {val:false};


gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects2Objects = Hashtable.newFrom({"r1": gdjs._484Code.GDr1Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects2Objects = Hashtable.newFrom({"r1": gdjs._484Code.GDr1Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects1Objects = Hashtable.newFrom({"r1": gdjs._484Code.GDr1Objects1});gdjs._484Code.eventsList0x755a04 = function(runtimeScene) {

{

gdjs._484Code.GDr1Objects2.createFrom(runtimeScene.getObjects("r1"));

gdjs._484Code.condition0IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects2Objects, runtimeScene, true, true);
}if (gdjs._484Code.condition0IsTrue_0.val) {
gdjs._484Code.GDflecher1Objects2.createFrom(runtimeScene.getObjects("flecher1"));
{for(var i = 0, len = gdjs._484Code.GDflecher1Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher1Objects2[i].hide();
}
}}

}


{

gdjs._484Code.GDr1Objects2.createFrom(runtimeScene.getObjects("r1"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects2Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
{gdjs._484Code.conditionTrue_1 = gdjs._484Code.condition1IsTrue_0;
gdjs._484Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7691340);
}
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
gdjs._484Code.GDflecher1Objects2.createFrom(runtimeScene.getObjects("flecher1"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._484Code.GDflecher1Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher1Objects2[i].hide(false);
}
}}

}


{

gdjs._484Code.GDr1Objects1.createFrom(runtimeScene.getObjects("r1"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
gdjs._484Code.condition2IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr1Objects1Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
gdjs._484Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._484Code.condition1IsTrue_0.val ) {
{
{gdjs._484Code.conditionTrue_1 = gdjs._484Code.condition2IsTrue_0;
gdjs._484Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7692284);
}
}}
}
if (gdjs._484Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


}; //End of gdjs._484Code.eventsList0x755a04
gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects2Objects = Hashtable.newFrom({"r3": gdjs._484Code.GDr3Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects2Objects = Hashtable.newFrom({"r3": gdjs._484Code.GDr3Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects1Objects = Hashtable.newFrom({"r3": gdjs._484Code.GDr3Objects1});gdjs._484Code.eventsList0x75616c = function(runtimeScene) {

{

gdjs._484Code.GDr3Objects2.createFrom(runtimeScene.getObjects("r3"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects2Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
{gdjs._484Code.conditionTrue_1 = gdjs._484Code.condition1IsTrue_0;
gdjs._484Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7693116);
}
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
gdjs._484Code.GDflecher3Objects2.createFrom(runtimeScene.getObjects("flecher3"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._484Code.GDflecher3Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher3Objects2[i].hide(false);
}
}}

}


{

gdjs._484Code.GDr3Objects2.createFrom(runtimeScene.getObjects("r3"));

gdjs._484Code.condition0IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects2Objects, runtimeScene, true, true);
}if (gdjs._484Code.condition0IsTrue_0.val) {
gdjs._484Code.GDflecher3Objects2.createFrom(runtimeScene.getObjects("flecher3"));
{for(var i = 0, len = gdjs._484Code.GDflecher3Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher3Objects2[i].hide();
}
}}

}


{

gdjs._484Code.GDr3Objects1.createFrom(runtimeScene.getObjects("r3"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr3Objects1Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
gdjs._484Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


}; //End of gdjs._484Code.eventsList0x75616c
gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects2Objects = Hashtable.newFrom({"r2": gdjs._484Code.GDr2Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects2Objects = Hashtable.newFrom({"r2": gdjs._484Code.GDr2Objects2});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects1Objects = Hashtable.newFrom({"r2": gdjs._484Code.GDr2Objects1});gdjs._484Code.eventsList0x7569ac = function(runtimeScene) {

{

gdjs._484Code.GDr2Objects2.createFrom(runtimeScene.getObjects("r2"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects2Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
{gdjs._484Code.conditionTrue_1 = gdjs._484Code.condition1IsTrue_0;
gdjs._484Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7695228);
}
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
gdjs._484Code.GDflecher2Objects2.createFrom(runtimeScene.getObjects("flecher2"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._484Code.GDflecher2Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher2Objects2[i].hide(false);
}
}}

}


{

gdjs._484Code.GDr2Objects2.createFrom(runtimeScene.getObjects("r2"));

gdjs._484Code.condition0IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects2Objects, runtimeScene, true, true);
}if (gdjs._484Code.condition0IsTrue_0.val) {
gdjs._484Code.GDflecher2Objects2.createFrom(runtimeScene.getObjects("flecher2"));
{for(var i = 0, len = gdjs._484Code.GDflecher2Objects2.length ;i < len;++i) {
    gdjs._484Code.GDflecher2Objects2[i].hide();
}
}}

}


{

gdjs._484Code.GDr2Objects1.createFrom(runtimeScene.getObjects("r2"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDr2Objects1Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
gdjs._484Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}}

}


}; //End of gdjs._484Code.eventsList0x7569ac
gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._484Code.GDRETOURSOMMAIREObjects1});gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._484Code.GDBACKObjects1});gdjs._484Code.eventsList0x5b71c8 = function(runtimeScene) {

{


gdjs._484Code.eventsList0x755a04(runtimeScene);
}


{


gdjs._484Code.eventsList0x75616c(runtimeScene);
}


{


gdjs._484Code.eventsList0x7569ac(runtimeScene);
}


{

gdjs._484Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
gdjs._484Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


gdjs._484Code.condition0IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._484Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{


gdjs._484Code.condition0IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._484Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "05", false);
}}

}


{

gdjs._484Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._484Code.condition0IsTrue_0.val = false;
gdjs._484Code.condition1IsTrue_0.val = false;
{
gdjs._484Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._484Code.mapOfGDgdjs_46_95484Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._484Code.condition0IsTrue_0.val ) {
{
gdjs._484Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._484Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


}; //End of gdjs._484Code.eventsList0x5b71c8


gdjs._484Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._484Code.GDFONDObjects1.length = 0;
gdjs._484Code.GDFONDObjects2.length = 0;
gdjs._484Code.GDFONDObjects3.length = 0;
gdjs._484Code.GDr2Objects1.length = 0;
gdjs._484Code.GDr2Objects2.length = 0;
gdjs._484Code.GDr2Objects3.length = 0;
gdjs._484Code.GDr3Objects1.length = 0;
gdjs._484Code.GDr3Objects2.length = 0;
gdjs._484Code.GDr3Objects3.length = 0;
gdjs._484Code.GDr1Objects1.length = 0;
gdjs._484Code.GDr1Objects2.length = 0;
gdjs._484Code.GDr1Objects3.length = 0;
gdjs._484Code.GDflecher3Objects1.length = 0;
gdjs._484Code.GDflecher3Objects2.length = 0;
gdjs._484Code.GDflecher3Objects3.length = 0;
gdjs._484Code.GDflecher2Objects1.length = 0;
gdjs._484Code.GDflecher2Objects2.length = 0;
gdjs._484Code.GDflecher2Objects3.length = 0;
gdjs._484Code.GDflecher1Objects1.length = 0;
gdjs._484Code.GDflecher1Objects2.length = 0;
gdjs._484Code.GDflecher1Objects3.length = 0;
gdjs._484Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._484Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._484Code.GDRETOURSOMMAIREObjects3.length = 0;
gdjs._484Code.GDBACKObjects1.length = 0;
gdjs._484Code.GDBACKObjects2.length = 0;
gdjs._484Code.GDBACKObjects3.length = 0;

gdjs._484Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_484Code'] = gdjs._484Code;
