gdjs._488Code = {};
gdjs._488Code.GDFONDObjects1= [];
gdjs._488Code.GDFONDObjects2= [];
gdjs._488Code.GDRETOURSOMMAIREObjects1= [];
gdjs._488Code.GDRETOURSOMMAIREObjects2= [];
gdjs._488Code.GDOBJET2Objects1= [];
gdjs._488Code.GDOBJET2Objects2= [];
gdjs._488Code.GDOBJET3Objects1= [];
gdjs._488Code.GDOBJET3Objects2= [];
gdjs._488Code.GDOBJET4Objects1= [];
gdjs._488Code.GDOBJET4Objects2= [];
gdjs._488Code.GDOBJET5Objects1= [];
gdjs._488Code.GDOBJET5Objects2= [];
gdjs._488Code.GDOBJET6Objects1= [];
gdjs._488Code.GDOBJET6Objects2= [];
gdjs._488Code.GDOBJET1Objects1= [];
gdjs._488Code.GDOBJET1Objects2= [];
gdjs._488Code.GDCIBLEOBJET2Objects1= [];
gdjs._488Code.GDCIBLEOBJET2Objects2= [];
gdjs._488Code.GDCIBLEOBJET3Objects1= [];
gdjs._488Code.GDCIBLEOBJET3Objects2= [];
gdjs._488Code.GDCIBLEOBJET4Objects1= [];
gdjs._488Code.GDCIBLEOBJET4Objects2= [];
gdjs._488Code.GDCIBLEOBJET6Objects1= [];
gdjs._488Code.GDCIBLEOBJET6Objects2= [];
gdjs._488Code.GDCIBLEOBJET5Objects1= [];
gdjs._488Code.GDCIBLEOBJET5Objects2= [];
gdjs._488Code.GDBACKObjects1= [];
gdjs._488Code.GDBACKObjects2= [];
gdjs._488Code.GDCIBLEOBJET1Objects1= [];
gdjs._488Code.GDCIBLEOBJET1Objects2= [];

gdjs._488Code.conditionTrue_0 = {val:false};
gdjs._488Code.condition0IsTrue_0 = {val:false};
gdjs._488Code.condition1IsTrue_0 = {val:false};
gdjs._488Code.condition2IsTrue_0 = {val:false};
gdjs._488Code.condition3IsTrue_0 = {val:false};
gdjs._488Code.condition4IsTrue_0 = {val:false};
gdjs._488Code.condition5IsTrue_0 = {val:false};
gdjs._488Code.condition6IsTrue_0 = {val:false};
gdjs._488Code.condition7IsTrue_0 = {val:false};
gdjs._488Code.conditionTrue_1 = {val:false};
gdjs._488Code.condition0IsTrue_1 = {val:false};
gdjs._488Code.condition1IsTrue_1 = {val:false};
gdjs._488Code.condition2IsTrue_1 = {val:false};
gdjs._488Code.condition3IsTrue_1 = {val:false};
gdjs._488Code.condition4IsTrue_1 = {val:false};
gdjs._488Code.condition5IsTrue_1 = {val:false};
gdjs._488Code.condition6IsTrue_1 = {val:false};
gdjs._488Code.condition7IsTrue_1 = {val:false};


gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._488Code.GDRETOURSOMMAIREObjects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET1Objects1Objects = Hashtable.newFrom({"OBJET1": gdjs._488Code.GDOBJET1Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET1Objects1Objects = Hashtable.newFrom({"CIBLEOBJET1": gdjs._488Code.GDCIBLEOBJET1Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET2Objects1Objects = Hashtable.newFrom({"OBJET2": gdjs._488Code.GDOBJET2Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET2Objects1Objects = Hashtable.newFrom({"CIBLEOBJET2": gdjs._488Code.GDCIBLEOBJET2Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET3Objects1Objects = Hashtable.newFrom({"OBJET3": gdjs._488Code.GDOBJET3Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET3Objects1Objects = Hashtable.newFrom({"CIBLEOBJET3": gdjs._488Code.GDCIBLEOBJET3Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET4Objects1Objects = Hashtable.newFrom({"OBJET4": gdjs._488Code.GDOBJET4Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET4Objects1Objects = Hashtable.newFrom({"CIBLEOBJET4": gdjs._488Code.GDCIBLEOBJET4Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET5Objects1Objects = Hashtable.newFrom({"OBJET5": gdjs._488Code.GDOBJET5Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET5Objects1Objects = Hashtable.newFrom({"CIBLEOBJET5": gdjs._488Code.GDCIBLEOBJET5Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET6Objects1Objects = Hashtable.newFrom({"OBJET6": gdjs._488Code.GDOBJET6Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET6Objects1Objects = Hashtable.newFrom({"CIBLEOBJET6": gdjs._488Code.GDCIBLEOBJET6Objects1});gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._488Code.GDBACKObjects1});gdjs._488Code.eventsList0x5b71c8 = function(runtimeScene) {

{


gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._488Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{

gdjs._488Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._488Code.condition0IsTrue_0.val = false;
gdjs._488Code.condition1IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._488Code.condition0IsTrue_0.val ) {
{
gdjs._488Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._488Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._488Code.GDCIBLEOBJET1Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET1"));
gdjs._488Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET1Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET1Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET1Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET1Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET1Objects1[i].setPosition(43,362);
}
}}

}


{

gdjs._488Code.GDCIBLEOBJET2Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET2"));
gdjs._488Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET2Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET2Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET2Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET2Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET2Objects1[i].setPosition(243,366);
}
}}

}


{

gdjs._488Code.GDCIBLEOBJET3Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET3"));
gdjs._488Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET3Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET3Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET3Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET3Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET3Objects1[i].setPosition(472,364);
}
}}

}


{

gdjs._488Code.GDCIBLEOBJET4Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET4"));
gdjs._488Code.GDOBJET4Objects1.createFrom(runtimeScene.getObjects("OBJET4"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET4Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET4Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET4Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET4Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET4Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET4Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET4Objects1[i].setPosition(693,372);
}
}}

}


{

gdjs._488Code.GDCIBLEOBJET5Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET5"));
gdjs._488Code.GDOBJET5Objects1.createFrom(runtimeScene.getObjects("OBJET5"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET5Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET5Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET5Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET5Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET5Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET5Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET5Objects1[i].setPosition(861,368);
}
}}

}


{

gdjs._488Code.GDCIBLEOBJET6Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET6"));
gdjs._488Code.GDOBJET6Objects1.createFrom(runtimeScene.getObjects("OBJET6"));

gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDOBJET6Objects1Objects, gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDCIBLEOBJET6Objects1Objects, false, runtimeScene, false);
}if (gdjs._488Code.condition0IsTrue_0.val) {
/* Reuse gdjs._488Code.GDOBJET6Objects1 */
{for(var i = 0, len = gdjs._488Code.GDOBJET6Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET6Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._488Code.GDOBJET6Objects1.length ;i < len;++i) {
    gdjs._488Code.GDOBJET6Objects1[i].setPosition(1068,372);
}
}}

}


{


gdjs._488Code.condition0IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._488Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "09", false);
}}

}


{

gdjs._488Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));
gdjs._488Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));
gdjs._488Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));
gdjs._488Code.GDOBJET4Objects1.createFrom(runtimeScene.getObjects("OBJET4"));
gdjs._488Code.GDOBJET5Objects1.createFrom(runtimeScene.getObjects("OBJET5"));
gdjs._488Code.GDOBJET6Objects1.createFrom(runtimeScene.getObjects("OBJET6"));

gdjs._488Code.condition0IsTrue_0.val = false;
gdjs._488Code.condition1IsTrue_0.val = false;
gdjs._488Code.condition2IsTrue_0.val = false;
gdjs._488Code.condition3IsTrue_0.val = false;
gdjs._488Code.condition4IsTrue_0.val = false;
gdjs._488Code.condition5IsTrue_0.val = false;
gdjs._488Code.condition6IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET1Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET1Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition0IsTrue_0.val = true;
        gdjs._488Code.GDOBJET1Objects1[k] = gdjs._488Code.GDOBJET1Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET1Objects1.length = k;}if ( gdjs._488Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET2Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET2Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition1IsTrue_0.val = true;
        gdjs._488Code.GDOBJET2Objects1[k] = gdjs._488Code.GDOBJET2Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET2Objects1.length = k;}if ( gdjs._488Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET3Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET3Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition2IsTrue_0.val = true;
        gdjs._488Code.GDOBJET3Objects1[k] = gdjs._488Code.GDOBJET3Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET3Objects1.length = k;}if ( gdjs._488Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET4Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET4Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition3IsTrue_0.val = true;
        gdjs._488Code.GDOBJET4Objects1[k] = gdjs._488Code.GDOBJET4Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET4Objects1.length = k;}if ( gdjs._488Code.condition3IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET5Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET5Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition4IsTrue_0.val = true;
        gdjs._488Code.GDOBJET5Objects1[k] = gdjs._488Code.GDOBJET5Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET5Objects1.length = k;}if ( gdjs._488Code.condition4IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._488Code.GDOBJET6Objects1.length;i<l;++i) {
    if ( !(gdjs._488Code.GDOBJET6Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._488Code.condition5IsTrue_0.val = true;
        gdjs._488Code.GDOBJET6Objects1[k] = gdjs._488Code.GDOBJET6Objects1[i];
        ++k;
    }
}
gdjs._488Code.GDOBJET6Objects1.length = k;}if ( gdjs._488Code.condition5IsTrue_0.val ) {
{
{gdjs._488Code.conditionTrue_1 = gdjs._488Code.condition6IsTrue_0;
gdjs._488Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7839508);
}
}}
}
}
}
}
}
if (gdjs._488Code.condition6IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{

gdjs._488Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._488Code.condition0IsTrue_0.val = false;
gdjs._488Code.condition1IsTrue_0.val = false;
{
gdjs._488Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._488Code.mapOfGDgdjs_46_95488Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._488Code.condition0IsTrue_0.val ) {
{
gdjs._488Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._488Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._488Code.eventsList0x5b71c8


gdjs._488Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._488Code.GDFONDObjects1.length = 0;
gdjs._488Code.GDFONDObjects2.length = 0;
gdjs._488Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._488Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._488Code.GDOBJET2Objects1.length = 0;
gdjs._488Code.GDOBJET2Objects2.length = 0;
gdjs._488Code.GDOBJET3Objects1.length = 0;
gdjs._488Code.GDOBJET3Objects2.length = 0;
gdjs._488Code.GDOBJET4Objects1.length = 0;
gdjs._488Code.GDOBJET4Objects2.length = 0;
gdjs._488Code.GDOBJET5Objects1.length = 0;
gdjs._488Code.GDOBJET5Objects2.length = 0;
gdjs._488Code.GDOBJET6Objects1.length = 0;
gdjs._488Code.GDOBJET6Objects2.length = 0;
gdjs._488Code.GDOBJET1Objects1.length = 0;
gdjs._488Code.GDOBJET1Objects2.length = 0;
gdjs._488Code.GDCIBLEOBJET2Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET2Objects2.length = 0;
gdjs._488Code.GDCIBLEOBJET3Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET3Objects2.length = 0;
gdjs._488Code.GDCIBLEOBJET4Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET4Objects2.length = 0;
gdjs._488Code.GDCIBLEOBJET6Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET6Objects2.length = 0;
gdjs._488Code.GDCIBLEOBJET5Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET5Objects2.length = 0;
gdjs._488Code.GDBACKObjects1.length = 0;
gdjs._488Code.GDBACKObjects2.length = 0;
gdjs._488Code.GDCIBLEOBJET1Objects1.length = 0;
gdjs._488Code.GDCIBLEOBJET1Objects2.length = 0;

gdjs._488Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_488Code'] = gdjs._488Code;
