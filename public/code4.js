gdjs._485Code = {};
gdjs._485Code.GDFONDObjects1= [];
gdjs._485Code.GDFONDObjects2= [];
gdjs._485Code.GDR3Objects1= [];
gdjs._485Code.GDR3Objects2= [];
gdjs._485Code.GDR2Objects1= [];
gdjs._485Code.GDR2Objects2= [];
gdjs._485Code.GDR1Objects1= [];
gdjs._485Code.GDR1Objects2= [];
gdjs._485Code.GDRETOURSOMMAIREObjects1= [];
gdjs._485Code.GDRETOURSOMMAIREObjects2= [];
gdjs._485Code.GDflecher3Objects1= [];
gdjs._485Code.GDflecher3Objects2= [];
gdjs._485Code.GDflecher2Objects1= [];
gdjs._485Code.GDflecher2Objects2= [];
gdjs._485Code.GDBACKObjects1= [];
gdjs._485Code.GDBACKObjects2= [];
gdjs._485Code.GDflecher1Objects1= [];
gdjs._485Code.GDflecher1Objects2= [];

gdjs._485Code.conditionTrue_0 = {val:false};
gdjs._485Code.condition0IsTrue_0 = {val:false};
gdjs._485Code.condition1IsTrue_0 = {val:false};
gdjs._485Code.condition2IsTrue_0 = {val:false};
gdjs._485Code.condition3IsTrue_0 = {val:false};
gdjs._485Code.conditionTrue_1 = {val:false};
gdjs._485Code.condition0IsTrue_1 = {val:false};
gdjs._485Code.condition1IsTrue_1 = {val:false};
gdjs._485Code.condition2IsTrue_1 = {val:false};
gdjs._485Code.condition3IsTrue_1 = {val:false};


gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._485Code.GDRETOURSOMMAIREObjects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._485Code.GDR1Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._485Code.GDR1Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._485Code.GDR1Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._485Code.GDR2Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._485Code.GDR2Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._485Code.GDR2Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._485Code.GDR3Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._485Code.GDR3Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._485Code.GDR3Objects1});gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._485Code.GDBACKObjects1});gdjs._485Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._485Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
gdjs._485Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._485Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._485Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._485Code.condition0IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects, runtimeScene, true, true);
}if (gdjs._485Code.condition0IsTrue_0.val) {
gdjs._485Code.GDflecher1Objects1.createFrom(runtimeScene.getObjects("flecher1"));
{for(var i = 0, len = gdjs._485Code.GDflecher1Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher1Objects1[i].hide();
}
}}

}


{

gdjs._485Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition1IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7706412);
}
}}
if (gdjs._485Code.condition1IsTrue_0.val) {
gdjs._485Code.GDflecher1Objects1.createFrom(runtimeScene.getObjects("flecher1"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._485Code.GDflecher1Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher1Objects1[i].hide(false);
}
}}

}


{

gdjs._485Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
gdjs._485Code.condition2IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR1Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
gdjs._485Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._485Code.condition1IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition2IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7707348);
}
}}
}
if (gdjs._485Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


{

gdjs._485Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._485Code.condition0IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects, runtimeScene, true, true);
}if (gdjs._485Code.condition0IsTrue_0.val) {
gdjs._485Code.GDflecher2Objects1.createFrom(runtimeScene.getObjects("flecher2"));
{for(var i = 0, len = gdjs._485Code.GDflecher2Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher2Objects1[i].hide();
}
}}

}


{

gdjs._485Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition1IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7708468);
}
}}
if (gdjs._485Code.condition1IsTrue_0.val) {
gdjs._485Code.GDflecher2Objects1.createFrom(runtimeScene.getObjects("flecher2"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._485Code.GDflecher2Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher2Objects1[i].hide(false);
}
}}

}


{

gdjs._485Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
gdjs._485Code.condition2IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR2Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
gdjs._485Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._485Code.condition1IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition2IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7709348);
}
}}
}
if (gdjs._485Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


{

gdjs._485Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._485Code.condition0IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects, runtimeScene, true, true);
}if (gdjs._485Code.condition0IsTrue_0.val) {
gdjs._485Code.GDflecher3Objects1.createFrom(runtimeScene.getObjects("flecher3"));
{for(var i = 0, len = gdjs._485Code.GDflecher3Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher3Objects1[i].hide();
}
}}

}


{

gdjs._485Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition1IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7710396);
}
}}
if (gdjs._485Code.condition1IsTrue_0.val) {
gdjs._485Code.GDflecher3Objects1.createFrom(runtimeScene.getObjects("flecher3"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._485Code.GDflecher3Objects1.length ;i < len;++i) {
    gdjs._485Code.GDflecher3Objects1[i].hide(false);
}
}}

}


{

gdjs._485Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
gdjs._485Code.condition2IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDR3Objects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
gdjs._485Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._485Code.condition1IsTrue_0.val ) {
{
{gdjs._485Code.conditionTrue_1 = gdjs._485Code.condition2IsTrue_0;
gdjs._485Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7711388);
}
}}
}
if (gdjs._485Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{


gdjs._485Code.condition0IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._485Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "06", false);
}}

}


{


gdjs._485Code.condition0IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._485Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{

gdjs._485Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._485Code.condition0IsTrue_0.val = false;
gdjs._485Code.condition1IsTrue_0.val = false;
{
gdjs._485Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._485Code.mapOfGDgdjs_46_95485Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._485Code.condition0IsTrue_0.val ) {
{
gdjs._485Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._485Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._485Code.eventsList0x5b71c8


gdjs._485Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._485Code.GDFONDObjects1.length = 0;
gdjs._485Code.GDFONDObjects2.length = 0;
gdjs._485Code.GDR3Objects1.length = 0;
gdjs._485Code.GDR3Objects2.length = 0;
gdjs._485Code.GDR2Objects1.length = 0;
gdjs._485Code.GDR2Objects2.length = 0;
gdjs._485Code.GDR1Objects1.length = 0;
gdjs._485Code.GDR1Objects2.length = 0;
gdjs._485Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._485Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._485Code.GDflecher3Objects1.length = 0;
gdjs._485Code.GDflecher3Objects2.length = 0;
gdjs._485Code.GDflecher2Objects1.length = 0;
gdjs._485Code.GDflecher2Objects2.length = 0;
gdjs._485Code.GDBACKObjects1.length = 0;
gdjs._485Code.GDBACKObjects2.length = 0;
gdjs._485Code.GDflecher1Objects1.length = 0;
gdjs._485Code.GDflecher1Objects2.length = 0;

gdjs._485Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_485Code'] = gdjs._485Code;
