gdjs.INTRO1Code = {};
gdjs.INTRO1Code.GDFONDObjects1= [];
gdjs.INTRO1Code.GDFONDObjects2= [];

gdjs.INTRO1Code.conditionTrue_0 = {val:false};
gdjs.INTRO1Code.condition0IsTrue_0 = {val:false};
gdjs.INTRO1Code.condition1IsTrue_0 = {val:false};


gdjs.INTRO1Code.eventsList0x5b71c8 = function(runtimeScene) {

{


gdjs.INTRO1Code.condition0IsTrue_0.val = false;
{
gdjs.INTRO1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.INTRO1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "INTRO2", true);
}}

}


}; //End of gdjs.INTRO1Code.eventsList0x5b71c8


gdjs.INTRO1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.INTRO1Code.GDFONDObjects1.length = 0;
gdjs.INTRO1Code.GDFONDObjects2.length = 0;

gdjs.INTRO1Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['INTRO1Code'] = gdjs.INTRO1Code;
