gdjs._495Code = {};
gdjs._495Code.GDFONDObjects1= [];
gdjs._495Code.GDFONDObjects2= [];
gdjs._495Code.GDRETOURSOMMAIREObjects1= [];
gdjs._495Code.GDRETOURSOMMAIREObjects2= [];
gdjs._495Code.GDOBJET2Objects1= [];
gdjs._495Code.GDOBJET2Objects2= [];
gdjs._495Code.GDCIBLEOBJET2Objects1= [];
gdjs._495Code.GDCIBLEOBJET2Objects2= [];
gdjs._495Code.GDOBJET3Objects1= [];
gdjs._495Code.GDOBJET3Objects2= [];
gdjs._495Code.GDCIBLEOBJET3Objects1= [];
gdjs._495Code.GDCIBLEOBJET3Objects2= [];
gdjs._495Code.GDOBJET1Objects1= [];
gdjs._495Code.GDOBJET1Objects2= [];
gdjs._495Code.GDBACKObjects1= [];
gdjs._495Code.GDBACKObjects2= [];
gdjs._495Code.GDCIBLEOBJET1Objects1= [];
gdjs._495Code.GDCIBLEOBJET1Objects2= [];

gdjs._495Code.conditionTrue_0 = {val:false};
gdjs._495Code.condition0IsTrue_0 = {val:false};
gdjs._495Code.condition1IsTrue_0 = {val:false};
gdjs._495Code.condition2IsTrue_0 = {val:false};
gdjs._495Code.condition3IsTrue_0 = {val:false};
gdjs._495Code.condition4IsTrue_0 = {val:false};
gdjs._495Code.conditionTrue_1 = {val:false};
gdjs._495Code.condition0IsTrue_1 = {val:false};
gdjs._495Code.condition1IsTrue_1 = {val:false};
gdjs._495Code.condition2IsTrue_1 = {val:false};
gdjs._495Code.condition3IsTrue_1 = {val:false};
gdjs._495Code.condition4IsTrue_1 = {val:false};


gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._495Code.GDRETOURSOMMAIREObjects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET1Objects1Objects = Hashtable.newFrom({"OBJET1": gdjs._495Code.GDOBJET1Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET1Objects1Objects = Hashtable.newFrom({"CIBLEOBJET1": gdjs._495Code.GDCIBLEOBJET1Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET2Objects1Objects = Hashtable.newFrom({"OBJET2": gdjs._495Code.GDOBJET2Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET2Objects1Objects = Hashtable.newFrom({"CIBLEOBJET2": gdjs._495Code.GDCIBLEOBJET2Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET3Objects1Objects = Hashtable.newFrom({"OBJET3": gdjs._495Code.GDOBJET3Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET3Objects1Objects = Hashtable.newFrom({"CIBLEOBJET3": gdjs._495Code.GDCIBLEOBJET3Objects1});gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._495Code.GDBACKObjects1});gdjs._495Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._495Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._495Code.condition0IsTrue_0.val = false;
gdjs._495Code.condition1IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._495Code.condition0IsTrue_0.val ) {
{
gdjs._495Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._495Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{


gdjs._495Code.condition0IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._495Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


gdjs._495Code.condition0IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._495Code.condition0IsTrue_0.val) {
gdjs._495Code.GDCIBLEOBJET1Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET1"));
gdjs._495Code.GDCIBLEOBJET2Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET2"));
gdjs._495Code.GDCIBLEOBJET3Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET3"));
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}{for(var i = 0, len = gdjs._495Code.GDCIBLEOBJET1Objects1.length ;i < len;++i) {
    gdjs._495Code.GDCIBLEOBJET1Objects1[i].hide();
}
}{for(var i = 0, len = gdjs._495Code.GDCIBLEOBJET2Objects1.length ;i < len;++i) {
    gdjs._495Code.GDCIBLEOBJET2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs._495Code.GDCIBLEOBJET3Objects1.length ;i < len;++i) {
    gdjs._495Code.GDCIBLEOBJET3Objects1[i].hide();
}
}}

}


{

gdjs._495Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));
gdjs._495Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));
gdjs._495Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._495Code.condition0IsTrue_0.val = false;
gdjs._495Code.condition1IsTrue_0.val = false;
gdjs._495Code.condition2IsTrue_0.val = false;
gdjs._495Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs._495Code.GDOBJET1Objects1.length;i<l;++i) {
    if ( !(gdjs._495Code.GDOBJET1Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._495Code.condition0IsTrue_0.val = true;
        gdjs._495Code.GDOBJET1Objects1[k] = gdjs._495Code.GDOBJET1Objects1[i];
        ++k;
    }
}
gdjs._495Code.GDOBJET1Objects1.length = k;}if ( gdjs._495Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._495Code.GDOBJET2Objects1.length;i<l;++i) {
    if ( !(gdjs._495Code.GDOBJET2Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._495Code.condition1IsTrue_0.val = true;
        gdjs._495Code.GDOBJET2Objects1[k] = gdjs._495Code.GDOBJET2Objects1[i];
        ++k;
    }
}
gdjs._495Code.GDOBJET2Objects1.length = k;}if ( gdjs._495Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._495Code.GDOBJET3Objects1.length;i<l;++i) {
    if ( !(gdjs._495Code.GDOBJET3Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._495Code.condition2IsTrue_0.val = true;
        gdjs._495Code.GDOBJET3Objects1[k] = gdjs._495Code.GDOBJET3Objects1[i];
        ++k;
    }
}
gdjs._495Code.GDOBJET3Objects1.length = k;}if ( gdjs._495Code.condition2IsTrue_0.val ) {
{
{gdjs._495Code.conditionTrue_1 = gdjs._495Code.condition3IsTrue_0;
gdjs._495Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7984740);
}
}}
}
}
if (gdjs._495Code.condition3IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{

gdjs._495Code.GDCIBLEOBJET1Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET1"));
gdjs._495Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));

gdjs._495Code.condition0IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET1Objects1Objects, gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET1Objects1Objects, false, runtimeScene, false);
}if (gdjs._495Code.condition0IsTrue_0.val) {
/* Reuse gdjs._495Code.GDCIBLEOBJET1Objects1 */
/* Reuse gdjs._495Code.GDOBJET1Objects1 */
{for(var i = 0, len = gdjs._495Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET1Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._495Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET1Objects1[i].setPosition((( gdjs._495Code.GDCIBLEOBJET1Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET1Objects1[0].getPointX("")),(( gdjs._495Code.GDCIBLEOBJET1Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET1Objects1[0].getPointY("")));
}
}}

}


{

gdjs._495Code.GDCIBLEOBJET2Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET2"));
gdjs._495Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));

gdjs._495Code.condition0IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET2Objects1Objects, gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET2Objects1Objects, false, runtimeScene, false);
}if (gdjs._495Code.condition0IsTrue_0.val) {
/* Reuse gdjs._495Code.GDCIBLEOBJET2Objects1 */
/* Reuse gdjs._495Code.GDOBJET2Objects1 */
{for(var i = 0, len = gdjs._495Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET2Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._495Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET2Objects1[i].setPosition((( gdjs._495Code.GDCIBLEOBJET2Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET2Objects1[0].getPointX("")),(( gdjs._495Code.GDCIBLEOBJET2Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET2Objects1[0].getPointY("")));
}
}}

}


{

gdjs._495Code.GDCIBLEOBJET3Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET3"));
gdjs._495Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._495Code.condition0IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDOBJET3Objects1Objects, gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDCIBLEOBJET3Objects1Objects, false, runtimeScene, false);
}if (gdjs._495Code.condition0IsTrue_0.val) {
/* Reuse gdjs._495Code.GDCIBLEOBJET3Objects1 */
/* Reuse gdjs._495Code.GDOBJET3Objects1 */
{for(var i = 0, len = gdjs._495Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET3Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._495Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._495Code.GDOBJET3Objects1[i].setPosition((( gdjs._495Code.GDCIBLEOBJET3Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET3Objects1[0].getPointX("")),(( gdjs._495Code.GDCIBLEOBJET3Objects1.length === 0 ) ? 0 :gdjs._495Code.GDCIBLEOBJET3Objects1[0].getPointY("")));
}
}}

}


{

gdjs._495Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._495Code.condition0IsTrue_0.val = false;
gdjs._495Code.condition1IsTrue_0.val = false;
{
gdjs._495Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._495Code.mapOfGDgdjs_46_95495Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._495Code.condition0IsTrue_0.val ) {
{
gdjs._495Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._495Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._495Code.eventsList0x5b71c8


gdjs._495Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._495Code.GDFONDObjects1.length = 0;
gdjs._495Code.GDFONDObjects2.length = 0;
gdjs._495Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._495Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._495Code.GDOBJET2Objects1.length = 0;
gdjs._495Code.GDOBJET2Objects2.length = 0;
gdjs._495Code.GDCIBLEOBJET2Objects1.length = 0;
gdjs._495Code.GDCIBLEOBJET2Objects2.length = 0;
gdjs._495Code.GDOBJET3Objects1.length = 0;
gdjs._495Code.GDOBJET3Objects2.length = 0;
gdjs._495Code.GDCIBLEOBJET3Objects1.length = 0;
gdjs._495Code.GDCIBLEOBJET3Objects2.length = 0;
gdjs._495Code.GDOBJET1Objects1.length = 0;
gdjs._495Code.GDOBJET1Objects2.length = 0;
gdjs._495Code.GDBACKObjects1.length = 0;
gdjs._495Code.GDBACKObjects2.length = 0;
gdjs._495Code.GDCIBLEOBJET1Objects1.length = 0;
gdjs._495Code.GDCIBLEOBJET1Objects2.length = 0;

gdjs._495Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_495Code'] = gdjs._495Code;
