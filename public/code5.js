gdjs._486Code = {};
gdjs._486Code.GDFONDObjects1= [];
gdjs._486Code.GDFONDObjects2= [];
gdjs._486Code.GDR2Objects1= [];
gdjs._486Code.GDR2Objects2= [];
gdjs._486Code.GDR3Objects1= [];
gdjs._486Code.GDR3Objects2= [];
gdjs._486Code.GDR1Objects1= [];
gdjs._486Code.GDR1Objects2= [];
gdjs._486Code.GDRETOURSOMMAIREObjects1= [];
gdjs._486Code.GDRETOURSOMMAIREObjects2= [];
gdjs._486Code.GDflecheR2Objects1= [];
gdjs._486Code.GDflecheR2Objects2= [];
gdjs._486Code.GDflecheR3Objects1= [];
gdjs._486Code.GDflecheR3Objects2= [];
gdjs._486Code.GDBACKObjects1= [];
gdjs._486Code.GDBACKObjects2= [];
gdjs._486Code.GDflecheR1Objects1= [];
gdjs._486Code.GDflecheR1Objects2= [];

gdjs._486Code.conditionTrue_0 = {val:false};
gdjs._486Code.condition0IsTrue_0 = {val:false};
gdjs._486Code.condition1IsTrue_0 = {val:false};
gdjs._486Code.condition2IsTrue_0 = {val:false};
gdjs._486Code.condition3IsTrue_0 = {val:false};
gdjs._486Code.conditionTrue_1 = {val:false};
gdjs._486Code.condition0IsTrue_1 = {val:false};
gdjs._486Code.condition1IsTrue_1 = {val:false};
gdjs._486Code.condition2IsTrue_1 = {val:false};
gdjs._486Code.condition3IsTrue_1 = {val:false};


gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._486Code.GDRETOURSOMMAIREObjects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._486Code.GDR2Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._486Code.GDR2Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects = Hashtable.newFrom({"R2": gdjs._486Code.GDR2Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._486Code.GDR3Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._486Code.GDR3Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects = Hashtable.newFrom({"R3": gdjs._486Code.GDR3Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._486Code.GDR1Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._486Code.GDR1Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects = Hashtable.newFrom({"R1": gdjs._486Code.GDR1Objects1});gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._486Code.GDBACKObjects1});gdjs._486Code.eventsList0x5b71c8 = function(runtimeScene) {

{

gdjs._486Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
gdjs._486Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._486Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._486Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
gdjs._486Code.condition2IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
gdjs._486Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._486Code.condition1IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition2IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6823236);
}
}}
}
if (gdjs._486Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


{

gdjs._486Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition1IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7089004);
}
}}
if (gdjs._486Code.condition1IsTrue_0.val) {
gdjs._486Code.GDflecheR2Objects1.createFrom(runtimeScene.getObjects("flecheR2"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._486Code.GDflecheR2Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR2Objects1[i].hide(false);
}
}}

}


{

gdjs._486Code.GDR2Objects1.createFrom(runtimeScene.getObjects("R2"));

gdjs._486Code.condition0IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR2Objects1Objects, runtimeScene, true, true);
}if (gdjs._486Code.condition0IsTrue_0.val) {
gdjs._486Code.GDflecheR2Objects1.createFrom(runtimeScene.getObjects("flecheR2"));
{for(var i = 0, len = gdjs._486Code.GDflecheR2Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR2Objects1[i].hide();
}
}}

}


{

gdjs._486Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition1IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7926900);
}
}}
if (gdjs._486Code.condition1IsTrue_0.val) {
gdjs._486Code.GDflecheR3Objects1.createFrom(runtimeScene.getObjects("flecheR3"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._486Code.GDflecheR3Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR3Objects1[i].hide(false);
}
}}

}


{

gdjs._486Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._486Code.condition0IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects, runtimeScene, true, true);
}if (gdjs._486Code.condition0IsTrue_0.val) {
gdjs._486Code.GDflecheR3Objects1.createFrom(runtimeScene.getObjects("flecheR3"));
{for(var i = 0, len = gdjs._486Code.GDflecheR3Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR3Objects1[i].hide();
}
}}

}


{

gdjs._486Code.GDR3Objects1.createFrom(runtimeScene.getObjects("R3"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
gdjs._486Code.condition2IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR3Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
gdjs._486Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._486Code.condition1IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition2IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6978468);
}
}}
}
if (gdjs._486Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ERREUR.wav", false, 100, 1);
}}

}


{

gdjs._486Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
gdjs._486Code.condition2IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
gdjs._486Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs._486Code.condition1IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition2IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7984604);
}
}}
}
if (gdjs._486Code.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{

gdjs._486Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
{gdjs._486Code.conditionTrue_1 = gdjs._486Code.condition1IsTrue_0;
gdjs._486Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7684148);
}
}}
if (gdjs._486Code.condition1IsTrue_0.val) {
gdjs._486Code.GDflecheR1Objects1.createFrom(runtimeScene.getObjects("flecheR1"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs._486Code.GDflecheR1Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR1Objects1[i].hide(false);
}
}}

}


{

gdjs._486Code.GDR1Objects1.createFrom(runtimeScene.getObjects("R1"));

gdjs._486Code.condition0IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDR1Objects1Objects, runtimeScene, true, true);
}if (gdjs._486Code.condition0IsTrue_0.val) {
gdjs._486Code.GDflecheR1Objects1.createFrom(runtimeScene.getObjects("flecheR1"));
{for(var i = 0, len = gdjs._486Code.GDflecheR1Objects1.length ;i < len;++i) {
    gdjs._486Code.GDflecheR1Objects1[i].hide();
}
}}

}


{


gdjs._486Code.condition0IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._486Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{


gdjs._486Code.condition0IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._486Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "07", false);
}}

}


{

gdjs._486Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._486Code.condition0IsTrue_0.val = false;
gdjs._486Code.condition1IsTrue_0.val = false;
{
gdjs._486Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._486Code.mapOfGDgdjs_46_95486Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._486Code.condition0IsTrue_0.val ) {
{
gdjs._486Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._486Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._486Code.eventsList0x5b71c8


gdjs._486Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._486Code.GDFONDObjects1.length = 0;
gdjs._486Code.GDFONDObjects2.length = 0;
gdjs._486Code.GDR2Objects1.length = 0;
gdjs._486Code.GDR2Objects2.length = 0;
gdjs._486Code.GDR3Objects1.length = 0;
gdjs._486Code.GDR3Objects2.length = 0;
gdjs._486Code.GDR1Objects1.length = 0;
gdjs._486Code.GDR1Objects2.length = 0;
gdjs._486Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._486Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._486Code.GDflecheR2Objects1.length = 0;
gdjs._486Code.GDflecheR2Objects2.length = 0;
gdjs._486Code.GDflecheR3Objects1.length = 0;
gdjs._486Code.GDflecheR3Objects2.length = 0;
gdjs._486Code.GDBACKObjects1.length = 0;
gdjs._486Code.GDBACKObjects2.length = 0;
gdjs._486Code.GDflecheR1Objects1.length = 0;
gdjs._486Code.GDflecheR1Objects2.length = 0;

gdjs._486Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_486Code'] = gdjs._486Code;
