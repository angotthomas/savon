gdjs._489Code = {};
gdjs._489Code.GDFONDObjects1= [];
gdjs._489Code.GDFONDObjects2= [];
gdjs._489Code.GDCIBLEOBJET2Objects1= [];
gdjs._489Code.GDCIBLEOBJET2Objects2= [];
gdjs._489Code.GDCIBLEOBJET3Objects1= [];
gdjs._489Code.GDCIBLEOBJET3Objects2= [];
gdjs._489Code.GDCIBLEOBJET1Objects1= [];
gdjs._489Code.GDCIBLEOBJET1Objects2= [];
gdjs._489Code.GDRETOURSOMMAIREObjects1= [];
gdjs._489Code.GDRETOURSOMMAIREObjects2= [];
gdjs._489Code.GDOBJET2Objects1= [];
gdjs._489Code.GDOBJET2Objects2= [];
gdjs._489Code.GDOBJET3Objects1= [];
gdjs._489Code.GDOBJET3Objects2= [];
gdjs._489Code.GDBACKObjects1= [];
gdjs._489Code.GDBACKObjects2= [];
gdjs._489Code.GDOBJET1Objects1= [];
gdjs._489Code.GDOBJET1Objects2= [];

gdjs._489Code.conditionTrue_0 = {val:false};
gdjs._489Code.condition0IsTrue_0 = {val:false};
gdjs._489Code.condition1IsTrue_0 = {val:false};
gdjs._489Code.condition2IsTrue_0 = {val:false};
gdjs._489Code.condition3IsTrue_0 = {val:false};
gdjs._489Code.condition4IsTrue_0 = {val:false};
gdjs._489Code.conditionTrue_1 = {val:false};
gdjs._489Code.condition0IsTrue_1 = {val:false};
gdjs._489Code.condition1IsTrue_1 = {val:false};
gdjs._489Code.condition2IsTrue_1 = {val:false};
gdjs._489Code.condition3IsTrue_1 = {val:false};
gdjs._489Code.condition4IsTrue_1 = {val:false};


gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDRETOURSOMMAIREObjects1Objects = Hashtable.newFrom({"RETOURSOMMAIRE": gdjs._489Code.GDRETOURSOMMAIREObjects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET3Objects1Objects = Hashtable.newFrom({"OBJET3": gdjs._489Code.GDOBJET3Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET3Objects1Objects = Hashtable.newFrom({"CIBLEOBJET3": gdjs._489Code.GDCIBLEOBJET3Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET2Objects1Objects = Hashtable.newFrom({"OBJET2": gdjs._489Code.GDOBJET2Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET2Objects1Objects = Hashtable.newFrom({"CIBLEOBJET2": gdjs._489Code.GDCIBLEOBJET2Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET1Objects1Objects = Hashtable.newFrom({"OBJET1": gdjs._489Code.GDOBJET1Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET1Objects1Objects = Hashtable.newFrom({"CIBLEOBJET1": gdjs._489Code.GDCIBLEOBJET1Objects1});gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDBACKObjects1Objects = Hashtable.newFrom({"BACK": gdjs._489Code.GDBACKObjects1});gdjs._489Code.eventsList0x5b71c8 = function(runtimeScene) {

{


gdjs._489Code.condition0IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs._489Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "C");
}}

}


{

gdjs._489Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));
gdjs._489Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));
gdjs._489Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._489Code.condition0IsTrue_0.val = false;
gdjs._489Code.condition1IsTrue_0.val = false;
gdjs._489Code.condition2IsTrue_0.val = false;
gdjs._489Code.condition3IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs._489Code.GDOBJET1Objects1.length;i<l;++i) {
    if ( !(gdjs._489Code.GDOBJET1Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._489Code.condition0IsTrue_0.val = true;
        gdjs._489Code.GDOBJET1Objects1[k] = gdjs._489Code.GDOBJET1Objects1[i];
        ++k;
    }
}
gdjs._489Code.GDOBJET1Objects1.length = k;}if ( gdjs._489Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._489Code.GDOBJET2Objects1.length;i<l;++i) {
    if ( !(gdjs._489Code.GDOBJET2Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._489Code.condition1IsTrue_0.val = true;
        gdjs._489Code.GDOBJET2Objects1[k] = gdjs._489Code.GDOBJET2Objects1[i];
        ++k;
    }
}
gdjs._489Code.GDOBJET2Objects1.length = k;}if ( gdjs._489Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs._489Code.GDOBJET3Objects1.length;i<l;++i) {
    if ( !(gdjs._489Code.GDOBJET3Objects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs._489Code.condition2IsTrue_0.val = true;
        gdjs._489Code.GDOBJET3Objects1[k] = gdjs._489Code.GDOBJET3Objects1[i];
        ++k;
    }
}
gdjs._489Code.GDOBJET3Objects1.length = k;}if ( gdjs._489Code.condition2IsTrue_0.val ) {
{
{gdjs._489Code.conditionTrue_1 = gdjs._489Code.condition3IsTrue_0;
gdjs._489Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6976316);
}
}}
}
}
if (gdjs._489Code.condition3IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "BATTERIE.wav", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "C");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "C");
}}

}


{


gdjs._489Code.condition0IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "C");
}if (gdjs._489Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "10", false);
}}

}


{

gdjs._489Code.GDRETOURSOMMAIREObjects1.createFrom(runtimeScene.getObjects("RETOURSOMMAIRE"));

gdjs._489Code.condition0IsTrue_0.val = false;
gdjs._489Code.condition1IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDRETOURSOMMAIREObjects1Objects, runtimeScene, true, false);
}if ( gdjs._489Code.condition0IsTrue_0.val ) {
{
gdjs._489Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._489Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", true);
}}

}


{

gdjs._489Code.GDCIBLEOBJET3Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET3"));
gdjs._489Code.GDOBJET3Objects1.createFrom(runtimeScene.getObjects("OBJET3"));

gdjs._489Code.condition0IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET3Objects1Objects, gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET3Objects1Objects, false, runtimeScene, false);
}if (gdjs._489Code.condition0IsTrue_0.val) {
/* Reuse gdjs._489Code.GDOBJET3Objects1 */
{for(var i = 0, len = gdjs._489Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET3Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._489Code.GDOBJET3Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET3Objects1[i].setPosition(320,347);
}
}}

}


{

gdjs._489Code.GDCIBLEOBJET2Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET2"));
gdjs._489Code.GDOBJET2Objects1.createFrom(runtimeScene.getObjects("OBJET2"));

gdjs._489Code.condition0IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET2Objects1Objects, gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET2Objects1Objects, false, runtimeScene, false);
}if (gdjs._489Code.condition0IsTrue_0.val) {
/* Reuse gdjs._489Code.GDOBJET2Objects1 */
{for(var i = 0, len = gdjs._489Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET2Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._489Code.GDOBJET2Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET2Objects1[i].setPosition(323,237);
}
}}

}


{

gdjs._489Code.GDCIBLEOBJET1Objects1.createFrom(runtimeScene.getObjects("CIBLEOBJET1"));
gdjs._489Code.GDOBJET1Objects1.createFrom(runtimeScene.getObjects("OBJET1"));

gdjs._489Code.condition0IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDOBJET1Objects1Objects, gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDCIBLEOBJET1Objects1Objects, false, runtimeScene, false);
}if (gdjs._489Code.condition0IsTrue_0.val) {
/* Reuse gdjs._489Code.GDOBJET1Objects1 */
{for(var i = 0, len = gdjs._489Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET1Objects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs._489Code.GDOBJET1Objects1.length ;i < len;++i) {
    gdjs._489Code.GDOBJET1Objects1[i].setPosition(326,144);
}
}}

}


{

gdjs._489Code.GDBACKObjects1.createFrom(runtimeScene.getObjects("BACK"));

gdjs._489Code.condition0IsTrue_0.val = false;
gdjs._489Code.condition1IsTrue_0.val = false;
{
gdjs._489Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs._489Code.mapOfGDgdjs_46_95489Code_46GDBACKObjects1Objects, runtimeScene, true, false);
}if ( gdjs._489Code.condition0IsTrue_0.val ) {
{
gdjs._489Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs._489Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SOMMAIRE", false);
}}

}


{


{
}

}


}; //End of gdjs._489Code.eventsList0x5b71c8


gdjs._489Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._489Code.GDFONDObjects1.length = 0;
gdjs._489Code.GDFONDObjects2.length = 0;
gdjs._489Code.GDCIBLEOBJET2Objects1.length = 0;
gdjs._489Code.GDCIBLEOBJET2Objects2.length = 0;
gdjs._489Code.GDCIBLEOBJET3Objects1.length = 0;
gdjs._489Code.GDCIBLEOBJET3Objects2.length = 0;
gdjs._489Code.GDCIBLEOBJET1Objects1.length = 0;
gdjs._489Code.GDCIBLEOBJET1Objects2.length = 0;
gdjs._489Code.GDRETOURSOMMAIREObjects1.length = 0;
gdjs._489Code.GDRETOURSOMMAIREObjects2.length = 0;
gdjs._489Code.GDOBJET2Objects1.length = 0;
gdjs._489Code.GDOBJET2Objects2.length = 0;
gdjs._489Code.GDOBJET3Objects1.length = 0;
gdjs._489Code.GDOBJET3Objects2.length = 0;
gdjs._489Code.GDBACKObjects1.length = 0;
gdjs._489Code.GDBACKObjects2.length = 0;
gdjs._489Code.GDOBJET1Objects1.length = 0;
gdjs._489Code.GDOBJET1Objects2.length = 0;

gdjs._489Code.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['_489Code'] = gdjs._489Code;
