gdjs.SOMMAIRECode = {};
gdjs.SOMMAIRECode.GDFONDObjects1= [];
gdjs.SOMMAIRECode.GDFONDObjects2= [];
gdjs.SOMMAIRECode.GDFONDObjects3= [];
gdjs.SOMMAIRECode.GDfctusageObjects1= [];
gdjs.SOMMAIRECode.GDfctusageObjects2= [];
gdjs.SOMMAIRECode.GDfctusageObjects3= [];
gdjs.SOMMAIRECode.GDfctestObjects1= [];
gdjs.SOMMAIRECode.GDfctestObjects2= [];
gdjs.SOMMAIRECode.GDfctestObjects3= [];
gdjs.SOMMAIRECode.GDfammatObjects1= [];
gdjs.SOMMAIRECode.GDfammatObjects2= [];
gdjs.SOMMAIRECode.GDfammatObjects3= [];
gdjs.SOMMAIRECode.GDevoObjects1= [];
gdjs.SOMMAIRECode.GDevoObjects2= [];
gdjs.SOMMAIRECode.GDevoObjects3= [];
gdjs.SOMMAIRECode.GDelementsObjects1= [];
gdjs.SOMMAIRECode.GDelementsObjects2= [];
gdjs.SOMMAIRECode.GDelementsObjects3= [];
gdjs.SOMMAIRECode.GDbesoinObjects1= [];
gdjs.SOMMAIRECode.GDbesoinObjects2= [];
gdjs.SOMMAIRECode.GDbesoinObjects3= [];
gdjs.SOMMAIRECode.GDchainesObjects1= [];
gdjs.SOMMAIRECode.GDchainesObjects2= [];
gdjs.SOMMAIRECode.GDchainesObjects3= [];
gdjs.SOMMAIRECode.GDprogObjects1= [];
gdjs.SOMMAIRECode.GDprogObjects2= [];
gdjs.SOMMAIRECode.GDprogObjects3= [];
gdjs.SOMMAIRECode.GDflechebesoinObjects1= [];
gdjs.SOMMAIRECode.GDflechebesoinObjects2= [];
gdjs.SOMMAIRECode.GDflechebesoinObjects3= [];
gdjs.SOMMAIRECode.GDflecheelementsObjects1= [];
gdjs.SOMMAIRECode.GDflecheelementsObjects2= [];
gdjs.SOMMAIRECode.GDflecheelementsObjects3= [];
gdjs.SOMMAIRECode.GDflecheevoObjects1= [];
gdjs.SOMMAIRECode.GDflecheevoObjects2= [];
gdjs.SOMMAIRECode.GDflecheevoObjects3= [];
gdjs.SOMMAIRECode.GDflechefammatObjects1= [];
gdjs.SOMMAIRECode.GDflechefammatObjects2= [];
gdjs.SOMMAIRECode.GDflechefammatObjects3= [];
gdjs.SOMMAIRECode.GDflechefctestObjects1= [];
gdjs.SOMMAIRECode.GDflechefctestObjects2= [];
gdjs.SOMMAIRECode.GDflechefctestObjects3= [];
gdjs.SOMMAIRECode.GDflecheprogObjects1= [];
gdjs.SOMMAIRECode.GDflecheprogObjects2= [];
gdjs.SOMMAIRECode.GDflecheprogObjects3= [];
gdjs.SOMMAIRECode.GDflechechainesObjects1= [];
gdjs.SOMMAIRECode.GDflechechainesObjects2= [];
gdjs.SOMMAIRECode.GDflechechainesObjects3= [];
gdjs.SOMMAIRECode.GDflechefctusageObjects1= [];
gdjs.SOMMAIRECode.GDflechefctusageObjects2= [];
gdjs.SOMMAIRECode.GDflechefctusageObjects3= [];

gdjs.SOMMAIRECode.conditionTrue_0 = {val:false};
gdjs.SOMMAIRECode.condition0IsTrue_0 = {val:false};
gdjs.SOMMAIRECode.condition1IsTrue_0 = {val:false};
gdjs.SOMMAIRECode.condition2IsTrue_0 = {val:false};
gdjs.SOMMAIRECode.condition3IsTrue_0 = {val:false};
gdjs.SOMMAIRECode.conditionTrue_1 = {val:false};
gdjs.SOMMAIRECode.condition0IsTrue_1 = {val:false};
gdjs.SOMMAIRECode.condition1IsTrue_1 = {val:false};
gdjs.SOMMAIRECode.condition2IsTrue_1 = {val:false};
gdjs.SOMMAIRECode.condition3IsTrue_1 = {val:false};


gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects2Objects = Hashtable.newFrom({"fctusage": gdjs.SOMMAIRECode.GDfctusageObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects2Objects = Hashtable.newFrom({"fctusage": gdjs.SOMMAIRECode.GDfctusageObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects1Objects = Hashtable.newFrom({"fctusage": gdjs.SOMMAIRECode.GDfctusageObjects1});gdjs.SOMMAIRECode.eventsList0x79028c = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDfctusageObjects2.createFrom(runtimeScene.getObjects("fctusage"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6901876);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "04", true);
}}

}


{

gdjs.SOMMAIRECode.GDfctusageObjects2.createFrom(runtimeScene.getObjects("fctusage"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(6966252);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefctusageObjects2.createFrom(runtimeScene.getObjects("flechefctusage"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefctusageObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefctusageObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDfctusageObjects1.createFrom(runtimeScene.getObjects("fctusage"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctusageObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefctusageObjects1.createFrom(runtimeScene.getObjects("flechefctusage"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefctusageObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefctusageObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x79028c
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects2Objects = Hashtable.newFrom({"fctest": gdjs.SOMMAIRECode.GDfctestObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects2Objects = Hashtable.newFrom({"fctest": gdjs.SOMMAIRECode.GDfctestObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects1Objects = Hashtable.newFrom({"fctest": gdjs.SOMMAIRECode.GDfctestObjects1});gdjs.SOMMAIRECode.eventsList0x78e184 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDfctestObjects2.createFrom(runtimeScene.getObjects("fctest"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7925020);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "05", true);
}}

}


{

gdjs.SOMMAIRECode.GDfctestObjects2.createFrom(runtimeScene.getObjects("fctest"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7926964);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefctestObjects2.createFrom(runtimeScene.getObjects("flechefctest"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefctestObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefctestObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDfctestObjects1.createFrom(runtimeScene.getObjects("fctest"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfctestObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefctestObjects1.createFrom(runtimeScene.getObjects("flechefctest"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefctestObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefctestObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x78e184
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects2Objects = Hashtable.newFrom({"fammat": gdjs.SOMMAIRECode.GDfammatObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects2Objects = Hashtable.newFrom({"fammat": gdjs.SOMMAIRECode.GDfammatObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects1Objects = Hashtable.newFrom({"fammat": gdjs.SOMMAIRECode.GDfammatObjects1});gdjs.SOMMAIRECode.eventsList0x6a7b94 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDfammatObjects2.createFrom(runtimeScene.getObjects("fammat"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7237324);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "07", true);
}}

}


{

gdjs.SOMMAIRECode.GDfammatObjects2.createFrom(runtimeScene.getObjects("fammat"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7057316);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefammatObjects2.createFrom(runtimeScene.getObjects("flechefammat"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefammatObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefammatObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDfammatObjects1.createFrom(runtimeScene.getObjects("fammat"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDfammatObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechefammatObjects1.createFrom(runtimeScene.getObjects("flechefammat"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechefammatObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechefammatObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x6a7b94
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects2Objects = Hashtable.newFrom({"evo": gdjs.SOMMAIRECode.GDevoObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects2Objects = Hashtable.newFrom({"evo": gdjs.SOMMAIRECode.GDevoObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects1Objects = Hashtable.newFrom({"evo": gdjs.SOMMAIRECode.GDevoObjects1});gdjs.SOMMAIRECode.eventsList0x779ed4 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDevoObjects2.createFrom(runtimeScene.getObjects("evo"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7089476);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "08", true);
}}

}


{

gdjs.SOMMAIRECode.GDevoObjects2.createFrom(runtimeScene.getObjects("evo"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8044956);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheevoObjects2.createFrom(runtimeScene.getObjects("flecheevo"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheevoObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheevoObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDevoObjects1.createFrom(runtimeScene.getObjects("evo"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDevoObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheevoObjects1.createFrom(runtimeScene.getObjects("flecheevo"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheevoObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheevoObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x779ed4
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects2Objects = Hashtable.newFrom({"elements": gdjs.SOMMAIRECode.GDelementsObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects2Objects = Hashtable.newFrom({"elements": gdjs.SOMMAIRECode.GDelementsObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects1Objects = Hashtable.newFrom({"elements": gdjs.SOMMAIRECode.GDelementsObjects1});gdjs.SOMMAIRECode.eventsList0x7b3584 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDelementsObjects2.createFrom(runtimeScene.getObjects("elements"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8067060);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "10", true);
}}

}


{

gdjs.SOMMAIRECode.GDelementsObjects2.createFrom(runtimeScene.getObjects("elements"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7979228);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheelementsObjects2.createFrom(runtimeScene.getObjects("flecheelements"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheelementsObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheelementsObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDelementsObjects1.createFrom(runtimeScene.getObjects("elements"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDelementsObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheelementsObjects1.createFrom(runtimeScene.getObjects("flecheelements"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheelementsObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheelementsObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x7b3584
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects2Objects = Hashtable.newFrom({"besoin": gdjs.SOMMAIRECode.GDbesoinObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects2Objects = Hashtable.newFrom({"besoin": gdjs.SOMMAIRECode.GDbesoinObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects1Objects = Hashtable.newFrom({"besoin": gdjs.SOMMAIRECode.GDbesoinObjects1});gdjs.SOMMAIRECode.eventsList0x7a05d4 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDbesoinObjects2.createFrom(runtimeScene.getObjects("besoin"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7973380);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "12", true);
}}

}


{

gdjs.SOMMAIRECode.GDbesoinObjects2.createFrom(runtimeScene.getObjects("besoin"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8041260);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechebesoinObjects2.createFrom(runtimeScene.getObjects("flechebesoin"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechebesoinObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechebesoinObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDbesoinObjects1.createFrom(runtimeScene.getObjects("besoin"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDbesoinObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechebesoinObjects1.createFrom(runtimeScene.getObjects("flechebesoin"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechebesoinObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechebesoinObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x7a05d4
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects2Objects = Hashtable.newFrom({"chaines": gdjs.SOMMAIRECode.GDchainesObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects2Objects = Hashtable.newFrom({"chaines": gdjs.SOMMAIRECode.GDchainesObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects1Objects = Hashtable.newFrom({"chaines": gdjs.SOMMAIRECode.GDchainesObjects1});gdjs.SOMMAIRECode.eventsList0x79cf1c = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDchainesObjects2.createFrom(runtimeScene.getObjects("chaines"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8078444);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "14", true);
}}

}


{

gdjs.SOMMAIRECode.GDchainesObjects2.createFrom(runtimeScene.getObjects("chaines"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7998836);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechechainesObjects2.createFrom(runtimeScene.getObjects("flechechaines"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechechainesObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechechainesObjects2[i].hide(false);
}
}}

}


{

gdjs.SOMMAIRECode.GDchainesObjects1.createFrom(runtimeScene.getObjects("chaines"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDchainesObjects1Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflechechainesObjects1.createFrom(runtimeScene.getObjects("flechechaines"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflechechainesObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflechechainesObjects1[i].hide();
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x79cf1c
gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects2Objects = Hashtable.newFrom({"prog": gdjs.SOMMAIRECode.GDprogObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects2Objects = Hashtable.newFrom({"prog": gdjs.SOMMAIRECode.GDprogObjects2});gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects1Objects = Hashtable.newFrom({"prog": gdjs.SOMMAIRECode.GDprogObjects1});gdjs.SOMMAIRECode.eventsList0x7aba64 = function(runtimeScene) {

{

gdjs.SOMMAIRECode.GDprogObjects2.createFrom(runtimeScene.getObjects("prog"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects2Objects, runtimeScene, true, true);
}if (gdjs.SOMMAIRECode.condition0IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheprogObjects2.createFrom(runtimeScene.getObjects("flecheprog"));
{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheprogObjects2.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheprogObjects2[i].hide();
}
}}

}


{

gdjs.SOMMAIRECode.GDprogObjects2.createFrom(runtimeScene.getObjects("prog"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition2IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
gdjs.SOMMAIRECode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects2Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition1IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition2IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7944628);
}
}}
}
if (gdjs.SOMMAIRECode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "15", true);
}}

}


{

gdjs.SOMMAIRECode.GDprogObjects1.createFrom(runtimeScene.getObjects("prog"));

gdjs.SOMMAIRECode.condition0IsTrue_0.val = false;
gdjs.SOMMAIRECode.condition1IsTrue_0.val = false;
{
gdjs.SOMMAIRECode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SOMMAIRECode.mapOfGDgdjs_46SOMMAIRECode_46GDprogObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SOMMAIRECode.condition0IsTrue_0.val ) {
{
{gdjs.SOMMAIRECode.conditionTrue_1 = gdjs.SOMMAIRECode.condition1IsTrue_0;
gdjs.SOMMAIRECode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8050740);
}
}}
if (gdjs.SOMMAIRECode.condition1IsTrue_0.val) {
gdjs.SOMMAIRECode.GDflecheprogObjects1.createFrom(runtimeScene.getObjects("flecheprog"));
{gdjs.evtTools.sound.playSound(runtimeScene, "bulle.wav", false, 100, 1);
}{for(var i = 0, len = gdjs.SOMMAIRECode.GDflecheprogObjects1.length ;i < len;++i) {
    gdjs.SOMMAIRECode.GDflecheprogObjects1[i].hide(false);
}
}}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x7aba64
gdjs.SOMMAIRECode.eventsList0x5b71c8 = function(runtimeScene) {

{


gdjs.SOMMAIRECode.eventsList0x79028c(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x78e184(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x6a7b94(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x779ed4(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x7b3584(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x7a05d4(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x79cf1c(runtimeScene);
}


{


gdjs.SOMMAIRECode.eventsList0x7aba64(runtimeScene);
}


{


{
}

}


}; //End of gdjs.SOMMAIRECode.eventsList0x5b71c8


gdjs.SOMMAIRECode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SOMMAIRECode.GDFONDObjects1.length = 0;
gdjs.SOMMAIRECode.GDFONDObjects2.length = 0;
gdjs.SOMMAIRECode.GDFONDObjects3.length = 0;
gdjs.SOMMAIRECode.GDfctusageObjects1.length = 0;
gdjs.SOMMAIRECode.GDfctusageObjects2.length = 0;
gdjs.SOMMAIRECode.GDfctusageObjects3.length = 0;
gdjs.SOMMAIRECode.GDfctestObjects1.length = 0;
gdjs.SOMMAIRECode.GDfctestObjects2.length = 0;
gdjs.SOMMAIRECode.GDfctestObjects3.length = 0;
gdjs.SOMMAIRECode.GDfammatObjects1.length = 0;
gdjs.SOMMAIRECode.GDfammatObjects2.length = 0;
gdjs.SOMMAIRECode.GDfammatObjects3.length = 0;
gdjs.SOMMAIRECode.GDevoObjects1.length = 0;
gdjs.SOMMAIRECode.GDevoObjects2.length = 0;
gdjs.SOMMAIRECode.GDevoObjects3.length = 0;
gdjs.SOMMAIRECode.GDelementsObjects1.length = 0;
gdjs.SOMMAIRECode.GDelementsObjects2.length = 0;
gdjs.SOMMAIRECode.GDelementsObjects3.length = 0;
gdjs.SOMMAIRECode.GDbesoinObjects1.length = 0;
gdjs.SOMMAIRECode.GDbesoinObjects2.length = 0;
gdjs.SOMMAIRECode.GDbesoinObjects3.length = 0;
gdjs.SOMMAIRECode.GDchainesObjects1.length = 0;
gdjs.SOMMAIRECode.GDchainesObjects2.length = 0;
gdjs.SOMMAIRECode.GDchainesObjects3.length = 0;
gdjs.SOMMAIRECode.GDprogObjects1.length = 0;
gdjs.SOMMAIRECode.GDprogObjects2.length = 0;
gdjs.SOMMAIRECode.GDprogObjects3.length = 0;
gdjs.SOMMAIRECode.GDflechebesoinObjects1.length = 0;
gdjs.SOMMAIRECode.GDflechebesoinObjects2.length = 0;
gdjs.SOMMAIRECode.GDflechebesoinObjects3.length = 0;
gdjs.SOMMAIRECode.GDflecheelementsObjects1.length = 0;
gdjs.SOMMAIRECode.GDflecheelementsObjects2.length = 0;
gdjs.SOMMAIRECode.GDflecheelementsObjects3.length = 0;
gdjs.SOMMAIRECode.GDflecheevoObjects1.length = 0;
gdjs.SOMMAIRECode.GDflecheevoObjects2.length = 0;
gdjs.SOMMAIRECode.GDflecheevoObjects3.length = 0;
gdjs.SOMMAIRECode.GDflechefammatObjects1.length = 0;
gdjs.SOMMAIRECode.GDflechefammatObjects2.length = 0;
gdjs.SOMMAIRECode.GDflechefammatObjects3.length = 0;
gdjs.SOMMAIRECode.GDflechefctestObjects1.length = 0;
gdjs.SOMMAIRECode.GDflechefctestObjects2.length = 0;
gdjs.SOMMAIRECode.GDflechefctestObjects3.length = 0;
gdjs.SOMMAIRECode.GDflecheprogObjects1.length = 0;
gdjs.SOMMAIRECode.GDflecheprogObjects2.length = 0;
gdjs.SOMMAIRECode.GDflecheprogObjects3.length = 0;
gdjs.SOMMAIRECode.GDflechechainesObjects1.length = 0;
gdjs.SOMMAIRECode.GDflechechainesObjects2.length = 0;
gdjs.SOMMAIRECode.GDflechechainesObjects3.length = 0;
gdjs.SOMMAIRECode.GDflechefctusageObjects1.length = 0;
gdjs.SOMMAIRECode.GDflechefctusageObjects2.length = 0;
gdjs.SOMMAIRECode.GDflechefctusageObjects3.length = 0;

gdjs.SOMMAIRECode.eventsList0x5b71c8(runtimeScene);
return;

}

gdjs['SOMMAIRECode'] = gdjs.SOMMAIRECode;
